# Official templates registry

## What are official templates?

This repository contains a curated set of templates hosted on Bitbucket and displayed in Bitbucket Pipelines UI. They are built to:

- Simplify configuring your pipeline by showcasing common use cases.
- Showcase a set of templates that are officially maintained: Atlassian works in collaboration with third-party vendors to ensure bugfixes and security updates are applied in a timely manner.

While it's recommended to have software authors maintaining their corresponding official Pipes, this is not a strict requirement. Creating and maintaining pipes is a public process, anyone can provide feedback, contribute code and suggest process changes.

## What do you mean by _official_?

In many cases, the pipes in this repository are not only supported but also *maintained* directly by the relevant vendors.

Others have been developed in collaboration with the company and reviewed by them.

## Contributing to the official templates

We'd love to have your contribution to this project. If you are a vendor and you would like to get involved in the project, please let us know on [pipelines-feedback@atlassian.com](mailto:pipelines-feedback@atlassian.com).
